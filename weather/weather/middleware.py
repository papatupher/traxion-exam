import asyncio, requests, datetime

from django.utils.decorators import sync_and_async_middleware
from django.conf import settings

from cities.models import City, Weather

from .views import get_weather_forecast

from ipware import get_client_ip
from django.contrib.gis.geos import Point
from django.contrib.gis.geoip2 import GeoIP2
g = GeoIP2(settings.GEOIP_PATH)


@sync_and_async_middleware
def request_and_response(get_response):
    # One-time configuration and initialization goes here.
    if asyncio.iscoroutinefunction(get_response):
        async def middleware(request):
            response = await get_response(request)
            return response

    else:
        def middleware(request):
            # lets not render the page without anything to show to the user
            # get this key in sessionn and get current user location
            if not 'user_location' in request.session:
                print("user_location not set")
            
                try:
                    # this block we use to try GeoIP2 to get current user location and 
                    # at this part of the develop we cant get the current user ip since we are 
                    # running at localhost
                    
                    ip, is_routable = get_client_ip(request)
                    print(f'ip {ip}')
                    print(f'is_routable {is_routable}')
                    
                    r = g.city(ip)
                    lat = r.get('latitude')
                    lon = r.get('longitude')
                    
                    weather_forecast = get_weather_forecast(r.get('city'), r.get('country_code'))
                    
                except Exception as e:
                    # as a workaround we manage to get the user current ip and location via sending some 
                    # request on other sites
                    
                    response = requests.get('http://jsonip.com')
                    ip = response.json()['ip']
                    url = f'http://ip-api.com/json/{ip}'
                    r = requests.get(url).json()
                    print(f'response at {url} >  \n {r}')
                    weather_forecast = get_weather_forecast(r.get('city'), r.get('countryCode'))
                    print(f'weather forecast > \n {weather_forecast}')
                    
                    lat = g.city(r.get('query')).get('latitude')
                    lon = g.city(r.get('query')).get('longitude')
                    
                city_data = weather_forecast.get('city')
                
                if city_data:
                    # upon getting the data from open weather we are going to save
                    # its coordinate as a Point field
                    # i encounter some troubles here since the usual format i seen is lat, lon
                    # but upon doing that its not locating on the current user location so we tried to reverse 
                    
                    loc = Point(city_data.get('coord').get('lon'), city_data.get('coord').get('lat'))
                    city, created = City.objects.get_or_create(name=city_data.get('name'), state_code=city_data.get('country'), location=loc)
                    
                    if created:
                        Weather.objects.create(city=city, data=weather_forecast)
                    else:
                        w = Weather.objects.filter(city=city, date=datetime.date.today())
                        if not w:
                            Weather.objects.create(city=city, data=weather_forecast, date=datetime.date.today())
                    
                    request.session['user_location'] = city.id
                    
                    # save the session valid in the end of the day so the user will never get the same
                    # data like yesterday                     
                    max_time = datetime.datetime.combine(datetime.date.today(), datetime.datetime.max.time())
                    
                    request.session.set_expiry(max_time)
                
            response = get_response(request)
    
                
            return response

    return middleware