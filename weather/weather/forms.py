from django.contrib.gis import forms


# class OSMWidget(forms.OSMWidget):
#     class Media:
#         js = ('openlayers.js')
        
class GeoForm(forms.Form):
    point = forms.PointField(widget=
        forms.OSMWidget(attrs={'map_width': '100%', 'map_height': '100%', 'supports_3d':True, 'default_lat':"14.599512", 'default_long':'120.984222'}))
    
    
    def __init__(self,*args,**kwargs):
        super (GeoForm,self ).__init__(*args,**kwargs)
        self.fields['point'].label = ''