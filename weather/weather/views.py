from django.shortcuts import render

from .forms import GeoForm

import datetime, json, requests
from datetime import date, timedelta

from cities.models import City, Weather
from cities.forms import CityForm


from django.contrib.gis.geoip2 import GeoIP2
from django.conf import settings
from django.core.serializers import serialize

from django.template.loader import render_to_string
from django.template import context,loader
from django.http import JsonResponse

from django.views import View



# get openweather data via city and code
def get_weather_forecast(city, state_code):
    url = f'http://api.openweathermap.org/data/2.5/forecast/daily?q={city}, {state_code}&units=metric&cnt=4&appid={settings.OPEN_WEATHER_KEY}'
    r = requests.get(url).json()
    return r

# get openweather data via lat and lon
def get_weather_forecast_latlon(lat, lon):
    url = f'http://api.openweathermap.org/data/2.5/forecast/daily?lat={lat}&lon={lon}&units=metric&cnt=4&appid={settings.OPEN_WEATHER_KEY}'
    r = requests.get(url).json()
    return r


class WeatherView(View):
    
    def setup(self, request, *args, **kwargs):
        if hasattr(self, 'get') and not hasattr(self, 'head'):
            self.head = self.get
        self.request = request
        self.args = args
        self.kwargs = kwargs
    
        
    def get(self, request, *args, **kwargs):
        # utilize select related this is how we do joins on django orm
        weather = Weather.objects.select_related('city').filter(city__id=self.request.session.get("user_location")).latest('date')
        
        context = {
            'obj':weather,
            'city':json.loads(serialize('geojson', [weather.city])),
            'form':CityForm(instance=weather.city),
            'data':weather.data,
		}
        return render(request, 'index.html', context=context)
    
    
    def post(self, request, *args, **kwargs):
        
        lat = request.POST.get('lat')
        lon = request.POST.get('lon')
        
        r = get_weather_forecast_latlon(lat,lon)
        
        weather_forecast = Weather.objects.filter(city__name=r['city']['name'], city__state_code=r['city']['country'], date=datetime.date.today()).first()
        print(weather_forecast)
        if not weather_forecast:
            print("NOT weather_forecast!")
            weather_forecast = Weather.create(r)
            print(f"weather_forecast {weather_forecast}")
        
        # render the fetch data into html 
        html = render_to_string('fragments/forecast.html', {
                'city':json.loads(serialize('geojson', [weather_forecast.city])),
                'data':weather_forecast.data,
        })
            
        return JsonResponse({"status":200, "data":html}, safe=False)
    
        