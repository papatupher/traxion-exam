from django.urls import path
from .views import CityMapView

app_name = "cities"

urlpatterns = [
    path("map/", CityMapView.as_view())
]