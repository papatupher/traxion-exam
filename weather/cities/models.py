from django.contrib.gis.db import models
from django.contrib.gis.geos import Point

from django.utils.formats import localize
from django.utils.dateparse import parse_datetime
import datetime
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

class City(models.Model):
    name = models.CharField(max_length=100)
    state_code = models.CharField(max_length=5)
    location = models.PointField()
    
    def __str__(self):
        return f'{self.name} {self.state_code}'

    class Meta:
        unique_together = ('name', 'state_code')
        
def data_default():
    return {}

def has_changed(instance, field):
    if not instance.pk:
        return False
    old_value = instance.__class__._default_manager.\
                filter(pk=instance.pk).values(field).get()[field]
    return not getattr(instance, field) == old_value

class Weather(models.Model):
    city    = models.ForeignKey(
                    City,
                    on_delete=models.CASCADE,
                    null=True, blank=True
                )
    data    = models.JSONField(default=data_default)
    date    = models.DateField(auto_now_add=True)

    
    class Meta:
        unique_together = ('city', 'date')
  
    def __str__(self):
        return f'{self.city} - {self.date}'
    
    def create(data):
        try:
            city_data = data.get('city')
            loc = Point(city_data.get('coord').get('lon'), city_data.get('coord').get('lat'))
            city = City.objects.create(name=data['city']['name'], state_code=data['city']['country'], location=loc)
            weather = Weather.objects.create(city=city, data=data)
            return weather
        except Exception as e:
            print(e)
            return False
        
    def save(self, *args, **kwargs): 
        # alter data field remove some data we dont need at the moment
        # watch out if there is some changes happen on data field upon saving
        
        if has_changed(self, 'data') or not self.pk:
        
            altered_data = {}
            forecast_aday = []
            
            today = ''
            data_list = self.data.get('list')
            for k, v in enumerate(data_list):
                weather = {}
                if len(v.get('weather')):
                    weather_data = v.get('weather')[0]
                    weather['main'] = weather_data.get('main')
                    weather['description'] = weather_data.get('description')
                    weather['icon'] = f"/static/img/{weather_data.get('icon')}.png"
                    
                date = parse_datetime(datetime.datetime.utcfromtimestamp(v.get('dt')).strftime('%Y-%m-%d %H:%M:%S'))
                
                if k == 0:
                    today = date
                    altered_data['temp'] = {
                        "morning":f'{v["temp"]["morn"]}°C',
                        "afternoon":f'{v["temp"]["day"]}°C',
                        "evening":f'{v["temp"]["eve"]}°C',
                        "night":f'{v["temp"]["night"]}°C',
                    }
                    altered_data['humidity'] = f'{v["humidity"]}%'
                    altered_data['weather'] = weather
                    altered_data['wind'] = f"{v.get('speed')} m/s"
                    
                short_day = (date + datetime.timedelta(days=k)).strftime("%a")
                temp = f'{v["temp"]["min"]}°C - {v["temp"]["max"]}'
                
                forecast_aday.append({
                    "weather":weather,
                    "forecast":f'{short_day} {temp}'
                })
            
            altered_data['day_name'] = today.strftime("%A")
            altered_data['date'] = localize(today)
            altered_data['country'] = f"{self.data.get('city').get('name')}, {self.data.get('city').get('country')}"
            altered_data['forecast_aday'] = forecast_aday
            
            self.data = altered_data
         
            
        super(Weather, self).save(*args, **kwargs)
        
        
    