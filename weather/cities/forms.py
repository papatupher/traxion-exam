from django.contrib.gis import forms
from .models import City

# class OSMWidget(forms.OSMWidget):
#     class Media:
#         js = ('openlayers.js')
        
class CityForm(forms.ModelForm):
    # point = forms.PointField(widget=
    #     forms.OSMWidget(attrs={'map_width': '100%', 'map_height': '100%', 'supports_3d':True, 'default_lat':"14.599512", 'default_long':'120.984222'}))
    
    class Meta:
        model = City
        fields = ('location',)
  
    def __init__(self,*args,**kwargs):
        super (CityForm,self ).__init__(*args,**kwargs)
        self.fields['location'].label = ''