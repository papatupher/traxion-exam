from django.contrib.gis import admin
from .models import City, Weather

@admin.register(City)
class CityAdmin(admin.OSMGeoAdmin):
    list_display = ("name", "state_code", "location")

admin.site.register(Weather)
# Register your models here.
