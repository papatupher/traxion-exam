import json
from django.shortcuts import render
from django.core.serializers import serialize
from django.views.generic import TemplateView

from .models import City


class CityMapView(TemplateView):
    template_name = "map.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cities = City.objects.all()
        context['cities'] = json.loads(serialize('geojson', cities))
        return context
